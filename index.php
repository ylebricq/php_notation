<?php

const NOM_FICHIER_ETU = "ListeEtudiantsS1.csv";
const CHEMIN = "fichiers_login/ListeEtudiantsS1.csv";
const CHEMIN_FICHIER_ADMIN = "fichiers_login/ListeAdministrateurVote.csv";


session_start();

if(isset($_POST['login']) && !empty($_POST['login'])) {

  $test = verif($_POST['login']);
  if($test == 1) {
    $_SESSION['login'] = $_POST['login'];
    header('Location: accueil.html');
  }

  else if($test == 2) {
    $_SESSION['admin'] = $_POST['login'];
    header('Location: admin.php');
  }

  else if($test == 0) {
    //echo "Identifiants incorrectes";
    //header('refresh:1;url=index.html');
    header('Location: index.php?error=1');
  }

}

else if(isset($_GET['error']) && $_GET['error']==1) {
  echo "
    <div class='alert alert-danger text-center'>
    <strong>Attention ! </strong> Login incorrect
    </div>";
}

else if(isset($_POST['deco'])) {
  session_unset();
  session_destroy();
  header('Location: index.php');
}

else{
  //echo "erreur de saisie";
  //header('refresh:1;url= index.html');
}




function verif($login_verif) {

  if (($fichier = fopen(CHEMIN_FICHIER_ADMIN, "r")) !== FALSE) {
    while (($logins = fgetcsv($fichier, 1000, ",")) !== FALSE) {
      foreach($logins as $login) {
        if($login == $login_verif){
          return 2;
        }
      }
    }
  fclose($fichier);

}

  if (($fichier = fopen(CHEMIN, "r")) !== FALSE) {
		while (($logins = fgetcsv($fichier, 1000, ",")) !== FALSE) {
			foreach($logins as $login) {
        if($login == $login_verif){
          return 1;
        }
      }
	  }
  fclose($fichier);
    return 0;
}
}
?>

<!--
<!DOCTYPE html>

<html lang="fr">

  <head>
	   <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
    <meta charset="utf-8">
    <title>Notation</title>
  </head>

  <body style="background-color:#ecf0f1">
    <div class="container" id="form-co">
      <h1 class="text-center mt-5">Notation des étudiants</h1>

      <form method="post" action="" class="form-group mt-4">
          <div class="text-center offset-1 col-10">
            <input type="text" name="login" placeholder="Login étudiant" class="form-control mx-auto form-control-lg text-center mt-2" style="width:200px;"/>
            <input type="submit" value="connexion" name="connexion" class="btn btn-success btn mt-4"/>
          </div>
      </form>
    </div>
  </body>

</html>
-->
<!DOCTYPE html>
<html lang="fr" >
<head>
  <meta charset="UTF-8">
  <title>Notation</title>
  <link rel="stylesheet" href="css/style.css">
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="js/form.js"></script>
  <link rel="icon" type="image/png" href="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQRh3g5gauQrAVmeiP612rEGsmQX25WPYZlDdiV319xvBIF2hjjzQ"/>
</head>

<body>
  <hgroup>
  <h1>Notation des étudiants</h1>
  <h3>Connectez vous pour pouvoir voter</h3>
</hgroup>
<form method="post" action="">
  <div class="group">
    <input type="password" name="login"><span class="highlight"></span><span class="bar"></span>
    <label>Login</label>
  </div>
  <button type="submit" class="button buttonBlue">Connexion
    <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
  </button>
</form>

</body>
</html>
