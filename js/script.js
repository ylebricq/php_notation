var app = angular.module('groupes', []);


app.controller("ListeController", ['$scope','$http',function($scope,$http){
  /*
  this.groupes = [
    "Groupe 1",
    "Groupe 2",
    "Groupe 3",
    "Groupe 4",
    "Groupe 5",
    "Groupe 6",
    "Groupe 7",
    "Groupe 8",
  ];
  */

  $scope.groupes = [];

  $http.get('api/getGroups.php')
    .then(function (response) {
      response.data.forEach(function(element) {
        $scope.groupes.unshift(element);
      });
      $scope.allVoted = false;
      if($scope.groupes.length == 0) {
        $scope.allVoted = true;
      };
    });



  console.log($scope.groupes);
  console.log($scope.groupes.length);

  //console.log($scope.groupes);
  $scope.buttonASpeach = "active";
  $scope.buttonBSpeach = "";
  $scope.buttonCSpeach = "";
  $scope.buttonDSpeach = "";
  $scope.buttonESpeach = "";
  $scope.slideSpeach = "A";
  $scope.choseSpeach = function(speachName){
    $scope.buttonASpeach = "";
    $scope.buttonBSpeach = "";
    $scope.buttonCSpeach = "";
    $scope.buttonDSpeach = "";
    $scope.buttonESpeach = "";
    if (speachName == 'A') {
      $scope.buttonASpeach = "active";
      $scope.slideSpeach = "A";
    }
    else if (speachName == 'B') {
      $scope.buttonBSpeach = "active";
      $scope.slideSpeach = "B";
    }
    else if (speachName == 'C') {
      $scope.buttonCSpeach = "active";
      $scope.slideSpeach = "C";
    }
    else if (speachName == 'D') {
      $scope.buttonDSpeach = "active";
      $scope.slideSpeach = "D";
    }
    else if (speachName == 'E') {
      $scope.buttonESpeach = "active";
      $scope.slideSpeach = "E";
    }
  };


  $scope.buttonASlide = "active";
  $scope.buttonBSlide = "";
  $scope.buttonCSlide = "";
  $scope.buttonDSlide = "";
  $scope.buttonESlide = "";
  $scope.slideChoice = "A";
  $scope.choseSlide = function(slideName){
    $scope.buttonASlide = "";
    $scope.buttonBSlide = "";
    $scope.buttonCSlide = "";
    $scope.buttonDSlide = "";
    $scope.buttonESlide = "";
    if (slideName == 'A') {
      $scope.buttonASlide = "active";
      $scope.slideChoice = "A";
    }
    else if (slideName == 'B') {
      $scope.buttonBSlide = "active";
      $scope.slideChoice = "B";
    }
    else if (slideName == 'C') {
      $scope.buttonCSlide = "active";
      $scope.slideChoice = "C";
    }
    else if (slideName == 'D') {
      $scope.buttonDSlide = "active";
      $scope.slideChoice = "D";
    }
    else if (slideName == 'E') {
      $scope.buttonESlide = "active";
      $scope.slideChoice = "E";
    }
  };

  $scope.buttonADemo = "active";
  $scope.buttonBDemo = "";
  $scope.buttonCDemo = "";
  $scope.buttonDDemo = "";
  $scope.buttonEDemo = "";
  $scope.demoChoice = "A";
  $scope.choseDemo = function(demoName){
    $scope.buttonADemo = "";
    $scope.buttonBDemo = "";
    $scope.buttonCDemo = "";
    $scope.buttonDDemo = "";
    $scope.buttonEDemo = "";
    if (demoName == 'A') {
      $scope.buttonADemo = "active";
      $scope.demoChoice = "A";
    }
    else if (demoName == 'B') {
      $scope.buttonBDemo = "active";
      $scope.demoChoice = "B";
    }
    else if (demoName == 'C') {
      $scope.buttonCDemo = "active";
      $scope.demoChoice = "C";
    }

    else if (demoName == 'D') {
      $scope.buttonDDemo = "active";
      $scope.demoChoice = "D";
    }

    else if (demoName == 'E') {
      $scope.buttonEDemo = "active";
      $scope.demoChoice = "E";
    }
  };

  $scope.buttonAQuestions = "active";
  $scope.buttonBQuestions = "";
  $scope.buttonCQuestions = "";
  $scope.buttonDQuestions = "";
  $scope.buttonEQuestions = "";
  $scope.questionsChoice = "A";
  $scope.choseQuestions = function(questionsName){
    $scope.buttonAQuestions = "";
    $scope.buttonBQuestions = "";
    $scope.buttonCQuestions = "";
    $scope.buttonDQuestions = "";
    $scope.buttonEQuestions = "";
    if (questionsName == 'A') {
      $scope.buttonAQuestions = "active";
      $scope.questionsChoice = "A";
    }
    else if (questionsName == 'B') {
      $scope.buttonBQuestions = "active";
      $scope.questionsChoice = "B";
    }
    else if (questionsName == 'C') {
      $scope.buttonCQuestions = "active";
      $scope.questionsChoice = "C";
    }
    else if (questionsName == 'D') {
      $scope.buttonDQuestions = "active";
      $scope.questionsChoice = "D";
    }
    else if (questionsName == 'E') {
      $scope.buttonEQuestions = "active";
      $scope.questionsChoice = "E";
    }
  };
}]);


app.directive('expand', function() {
  return {
    restrict: 'EA',
    templateUrl: 'liste.html',
    replace: true,
    transclude: true,

    link: function(scope, element, attrs){
      temp = scope;
      scope.showMe = false;
      scope.toggle = function toggle(groupe){
        console.log(scope);
        console.log(temp);

          if(scope != temp) {
            console.log("click");
            scope.showMe = !scope.showMe;
            temp.showMe = false;
          }
          else if(scope == temp) {
            scope.showMe = !scope.showMe;
          }
          scope.groupChoice = groupe;
          temp = scope;
      };
    }

  };
});
