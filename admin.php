<?php

session_start();


if(!isset($_SESSION['admin'])) {
  header('Location: index.php');
}

$directory = "fichiers_notes";

$infosGroupes = array("Gestion des projets tutorés", "Interface web QCM", "YML - VM", "Jeu en ligne", "Pointeurs", "Kinect - Unity", "Machine learning", "Interface réseau");

$tab_nom_fichiers = getFiles($directory);
$liste1 = array();
$liste2 = array();
$liste3 = array();
$liste4 = array();
$liste5 = array();
$liste6 = array();
$liste7 = array();
$liste8 = array();

if(!empty($tab_nom_fichiers)) {

  foreach ($tab_nom_fichiers as $nom_fichier) {
    //$cle = strtok($nom_fichier, '-');
    $n = str_split($nom_fichier);
    $data = file($directory.'/'.$nom_fichier);
    $note = $data[1];

    $n = $n[6];

    switch ($n) {
      case 1:
          $liste1[] = $note;
          break;
      case 2:
          $liste2[] = $note;
          break;
      case 3:
          $liste3[] = $note;
          break;
      case 4:
          $liste4[] = $note;
          break;
      case 5:
          $liste5[] = $note;
          break;
      case 6:
          $liste6[] = $note;
          break;
      case 7:
          $liste7[] = $note;
          break;
      case 8:
          $liste8[] = $note;
          break;
    }
  }
}
$tab_notes_groupes = array($liste1, $liste2, $liste3, $liste4, $liste5, $liste6, $liste7, $liste8);
$_SESSION['notes'] = $tab_notes_groupes;

$tab_moyenne_groupes = moyenne_groupes($tab_notes_groupes);
$_SESSION['moyennes'] = $tab_moyenne_groupes;

function getFiles($directory) {
  if (is_dir($directory)){
    if ($dh = opendir($directory)){
      while (($file = readdir($dh)) !== false){
        if ($file != "." && $file != "..") {
          $tab_nom_fichiers[] = $file;
        }
      }
      closedir($dh);
    }
  }

  if(!is_null($tab_nom_fichiers)) {asort($tab_nom_fichiers);}
  return $tab_nom_fichiers;
}


function moyenne_groupes($tab_notes_groupes) {
  $i = 0;
  foreach ($tab_notes_groupes as $notes) {
    if(sizeof($notes) == 0) {
      $tab_moyenne_groupes[$i] = array_sum($notes)/1;
    }
    else {
      $tab_moyenne_groupes[$i] = array_sum($notes)/sizeof($notes);
    }

    $i++;
  }
  return $tab_moyenne_groupes;
}


?>


<html lang="fr">

  <head>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
  <script src="js/script.js"></script>
    <meta charset="utf-8">
    <title>Notation</title>
  </head>

  <div class="container">
  <body class="p-3 mb-2 bg-light text-dark" ng-app="groupes" ng-controller="ListeController as listeCtrl" style="background-color:#ecf0f1">


  <h1 class="row"> Notes des groupes </h1>

  <form method="post" action="index.php" class="form-group">
    <input type="submit" value="Déconnexion" name="deco" class="btn btn-primary btn-sm"/>
  </form>

  <!--<h4 ng-repeat="groupe in listeCtrl.groupes"> {{groupe}}  </h4> -->

  <h4>
    <table class='table' style="background-color:white; box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);">
      <thead>
        <tr>
          <th>Groupe</th>
          <th>Moyenne du groupe</th>
        </tr>
      </thead>
      <tbody>
    <?php

      for($i = 1; $i < 9; $i++) {
        //echo "Groupe ".$i." ".$_SESSION['moyennes'][$i-1]."<br/>";
          echo "
              <tr class='info'>
                <td>Groupe ".$i." : ".$infosGroupes[$i-1]."</td>
                <td>".round($_SESSION['moyennes'][$i-1],2)."</td>
            </tr>";

      }
    ?>
    </tbody>
  </table>

  </h4>

    </form>
    </body>
  </div>

</html>
