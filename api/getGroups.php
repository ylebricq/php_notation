<?php
session_start();

if(!isset($_SESSION['login'])) {
  header('Location: ../index.html');
}


$groups = array();

$infosGroupes = array("Gestion des projets tutorés", "Interface web QCM", "YML - VM", "Jeu en ligne", "Pointeurs", "Kinect - Unity", "Machine learning", "Interface réseau");

$login = $_SESSION['login'];

for ($i=8; $i > 0; $i--) {
  $filename = '../fichiers_notes/groupe'.$i.'-'.$login.'.csv';

  if (!file_exists($filename)) {
    //$group = "Groupe ".$i;
    $group = "Groupe ".$i." : ".$infosGroupes[$i-1];
    array_push($groups,$group);
  }
}

echo json_encode($groups);


?>
