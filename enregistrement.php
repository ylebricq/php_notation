<?php

  session_start();

  if(!isset($_SESSION['login'])) {
    header('Location: index.php');
  }

  const A = 5;
  const B = 3.5;
  const C = 2.5;
  const D = 1.5;
  const E = 1;


  $login = $_SESSION['login'];

  //$n = substr($_POST['groupChoice'], strrpos($_POST['groupChoice'], " ") + 1);
  $n = substr($_POST['groupChoice'],7,1);
  $nom_fichier = "groupe".$n."-".$login.'.csv';
  $chemin = "fichiers_notes/".$nom_fichier;


  $note = constant($_POST['slideSpeach']) + constant($_POST['slideChoice']) + constant($_POST['demoChoice']) + constant($_POST['questionsChoice']);

  $lignes = array(array($_POST['slideSpeach'],$_POST['slideChoice'],$_POST['demoChoice'],$_POST['questionsChoice']), array($note));

  $file = fopen($chemin, 'w');

  //echo $nom_fichier;
  //echo $chemin;

  foreach ($lignes as $ligne) {
    fputcsv($file, $ligne);
  }

  header('Location: accueil.html');

?>
